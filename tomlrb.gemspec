#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: tomlrb 1.3.0 ruby lib

Gem::Specification.new do |s|
  s.name = "tomlrb".freeze
  s.version = "1.3.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Francois Bernier".freeze]
  s.date = "2020-03-19"
  s.description = "A racc based toml parser".freeze
  s.email = ["frankbernier@gmail.com".freeze]
  s.files = ["LICENSE.txt".freeze, "lib/tomlrb.rb".freeze, "lib/tomlrb/generated_parser.rb".freeze, "lib/tomlrb/handler.rb".freeze, "lib/tomlrb/parser.rb".freeze, "lib/tomlrb/parser.y".freeze, "lib/tomlrb/scanner.rb".freeze, "lib/tomlrb/string_utils.rb".freeze, "lib/tomlrb/version.rb".freeze]
  s.homepage = "https://github.com/fbernier/tomlrb".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.0".freeze)
  s.rubygems_version = "2.5.2.1".freeze
  s.summary = "A racc based toml parser".freeze
end
